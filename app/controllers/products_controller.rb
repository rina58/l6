class ProductsController < ApplicationController
    def index
        @products = Product.all
    end
    
    def new
        @product = Product.new
    end
    
    def create
        @product = Product.new(name: params[:product][:name], price: params[:product][:price])
        if @product.save
            
            flash[:success] = "登録完了"
            redirect_to products_path
        else
            flash[:failed] = "登録失敗"
            render 'new'
        end
    end
    
    def destroy
        product = Product.find(params[:id])
        product.destroy
           
        redirect_to products_path, flash: {destroy:  "ツイートを削除しました。"}
    end
end
