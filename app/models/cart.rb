class Cart < ApplicationRecord
    has_many :cart_items, dependent: :destroy
     def add_product(product_id)
        current_item = cart_items.find_by(product_id)
        if current_item
            current_item.qty += 1
        else
            current_item = cart_items.build(product_id: product_id)
        end
        current_item
     end
     
     
end
